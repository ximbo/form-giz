$(function(){
    var houses = $('#houses');
    var flats = $('#flats');
    var form = $('#form');

    houses.select2();

    var fs = houses.find(':selected').data('flats');
    flats.html('');
    for (var i = 1; i <= fs; i++) {
        flats.append($('<option>', {
            value: i,
            text: i
        }));
    }

    houses.on('change', function (e) {
        var fs = $(this).find(':selected').data('flats');
        flats.html('');
        for (var i = 1; i <= fs; i++) {
            flats.append($('<option>', {
                value: i,
                text: i
            }));
        }
    });

    form.on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.post('/send', data)
            .done(function (response) {
                response = $.parseJSON(response);
                if (response.success) {
                    alert('Сохранено');
                } else {
                    alert('Что-то пошло не так');
                }
            })
            .fail(function () {
                alert('Ошибка при передаче данных');
            });
        ;
    });
});