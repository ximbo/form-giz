<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    $models = $this->models->all();

    return $this->renderer->render($response, 'index.phtml', compact('models'));
});

$app->post('/send', function (Request $request, Response $response, $args) {
    $post = $request->getParsedBody();
    $model = $this->models->get($post['house']);
    $flat = $post['flat'];
    $string = "{$model->address};{$flat}";
    $this->file->write($string);

    return ($request->isXhr()) ?
        json_encode(['success' => true]) :
        $response->withRedirect('/');
});

$app->map(['GET', 'POST'], '/query', function (Request $request, Response $response, $args) {
    $params = ($request->isGet()) ? $request->getQueryParams() : $request->getParsedBody();
    $query = $params['query'];
    if (empty($query)) return $response->withRedirect('/');
    $models = $this->models->search($query);

    return ($request->isXhr()) ?
        json_encode($models) :
        $this->renderer->render($response, 'index.phtml', compact('models'));
});
