<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Файл для записи
$container['file'] = function ($c) {
    $settings = $c->get('settings')['file'];
    return new App\File($settings['path']);
};

// PDO
$container['pdo'] = function ($c) {
    $settings = $c->get('settings')['db'];
    $dsn = 'sqlite:' . $settings['path'];
    return new PDO($dsn);
};

// Мапперы
$container['models'] = function ($c) {
    return new App\Mappers\ModelMapper($c->pdo);
};
