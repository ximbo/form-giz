<?php

namespace App\Mappers;

use \PDO;
use App\Models\Model;

class ModelMapper
{
    /* @var PDO */
    protected $pdo;

    protected $table = 'houses';

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function get($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = :id";
        $sth = $this->pdo->prepare($sql);
        $sth->execute([':id' => $id]);
        return new Model($sth->fetch(PDO::FETCH_ASSOC));
    }

    /**
     * @return Model[]
     */
    public function all()
    {
        $sql = "SELECT * FROM {$this->table}";
        $sth = $this->pdo->prepare($sql);
        $sth->execute();
        return Model::collection($sth->fetchAll(PDO::FETCH_ASSOC));
    }

    /**
     * @param $query
     * @return Model[]
     */
    public function search($query)
    {
        $sql = "SELECT * FROM {$this->table} WHERE address LIKE :address";
        $sth = $this->pdo->prepare($sql);
        $sth->execute([':address' => "%{$query}%"]);
        return Model::collection($sth->fetchAll(PDO::FETCH_ASSOC));
    }
}
