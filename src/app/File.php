<?php

namespace App;

class File
{
    /* @var string */
    protected $file;

    public function __construct($file = '')
    {
        $this->file = $file;
    }

    /**
     * Запись в файл
     *
     * @param string $string
     */
    public function write($string)
    {
        $file = fopen($this->file, 'a');
        fwrite($file, $string . PHP_EOL);
        fclose($file);
    }
}
