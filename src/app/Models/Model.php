<?php

namespace App\Models;

class Model
{
    /* @var int */
    public $id;

    /* @var string */
    public $address;

    /* @var int */
    public $flats;

    public function __construct(array $attributes = [])
    {
        $this->fromArray($attributes);
    }

    /**
     * @param string $attribute
     * @return mixed
     */
    public function get($attribute)
    {
        return $this->{$attribute};
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return Model
     */
    public function set($attribute, $value)
    {
        $this->{$attribute} = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int
     */
    public function getFlats()
    {
        return $this->flats;
    }

    /**
     * @param int $flats
     * @return Model
     */
    public function setFlats($flats)
    {
        $this->flats = $flats;
        return $this;
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function fromArray(array $attributes = [])
    {
        foreach ($attributes as $key => $value) {
            $this->set($key, $value);
        }
        return $this;
    }

    /**
     * @param array $properties
     * @return Model[]
     */
    public static function collection(array $properties = [])
    {
        $collection = [];
        foreach ($properties as $attributes) {
            $collection[] = new static($attributes);
        }
        return $collection;
    }
}
