<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        // Db settings
        'db' => [
            'path' => __DIR__ . '/../database/db.sqlite',
        ],

        //File setting
        'file' => [
            'path' => __DIR__ . '/../files/file.txt',
        ],
    ],
];
