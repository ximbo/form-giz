# Форма

Клонируешь git clone https://ximbo@bitbucket.org/ximbo/form-giz.git

Или из архива разворачивашь

## Установка композера

    https://getcomposer.org/doc/00-intro.md#installation-windows

## Установка зависимостей

Запускаешь команду

    composer install

## Запуск локального сервера

    php -S 0.0.0.0:8080 -t public public/index.php

Открываешь в браузере http://0.0.0.0:8080/
